(map!
 :ne "C-w <right>" #'evil-window-right
 :ne "C-w <left>"  #'evil-window-left
 :ne "C-w <up>"    #'evil-window-up
 :ne "C-w <down>"  #'evil-window-down)

(map! :leader
      (:prefix "w"
        :desc "Ace window switch" :n "o" #'other-window))

(map!
 (:leader
   (:prefix "w"
     :desc "vsplit" :nv "/" #'evil-window-vsplit
     :desc "hsplit" :nv "-" #'evil-window-split)))

(map!
 (:leader
   (:prefix "w"
     :desc "Delete window" :n "d" #'evil-window-delete
     :desc "Kill Win+Buf"  :n "q" #'kill-buffer-and-window)))

(map!
 (:leader
   (:prefix "b"
     :desc "Kill Buffer" :n "d" #'kill-this-buffer
     :desc "Kill buffer" :n "q" #'kill-this-buffer)))

(defun rncwind-visit-config ()
  (interactive)
  (find-file "~/.doom.d/config.org"))

(map! (:leader (:prefix "h"
                 :desc "Open config.org" :n "C" #'rncwind-visit-config)))

(defun rncwind-reload-config ()
  (interactive)
  (org-babel-load-file (expand-file-name "~/.doom.d/config.org")))

(map! (:leader (:prefix "h"
                 :desc "Reload config.org" :n "R" #'rncwind-reload-config)))

(setq doom-font (font-spec :family "hack" :size 14)
      doom-variable-pitch-font (font-spec :family "Noto Sans")
      doom-unicode-font (font-spec :family "hack")
      doom-big-font (font-spec :family "hack" :size 20))

(setq doom-theme 'doom-vibrant)

(setq-default doom-line-numbers-style 'relative)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

(map! :leader
      (:prefix "f"
        :desc "Find File" :nv "f" #'counsel-find-file))

(map! :leader
      (:prefix "b"
        :desc "List Buffers" :nv "b" #'ivy-switch-buffer))

(map! :leader
      (:prefix "p"
        :desc "Projectile find file" :nv "f" #'counsel-projectile-find-file))

(setq-default +cc-default-compiler-options
              '((c-mode   "-std=c11" nil)
                (c++-mode "-std=c++17" nil)))

(c-add-style "rncwnd"
             '("bsd"
               (c-basic-offset . 4)
               (tab-width . 4)
               (c-offsets-alist
                (access-label . --))))
(setq-default c-default-style "rncwnd")

(map! :after cc-mode
      :map c++-mode-map
      :localleader
      (:desc "clang-format" :prefix "f"
        :desc "clang-format Buffer" :n "b" #'clang-format-buffer
        :desc "clang-format Region" :n "r" #'clang-format-region))

(add-hook 'c++-mode-hook 'yas-minor-mode-on)
(add-hook 'c-mode-hook 'yas-minor-mode-on)

(map! :after inf-ruby
      :map inf-ruby-mode-map
      :localleader
      (:desc "REPL" :n "'" #'inf-ruby)

      (:desc "Eval" :prefix "e"
        :desc "Eval defun"  :n "f" #'ruby-send-definition
        :desc "Eval region" :n "r" #'ruby-send-region))

(add-hook 'ruby-mode-hook 'yas-minor-mode-on)

(setq inferior-lisp-program "/usr/bin/sbcl")
(setq slime-contribs '(slime-fancy slime-company))

(map! :map lisp-mode-map
      :localleader
      (:desc "SLIME" :n "'" #'slime
       :desc "Quit slime" :n "q" #'slime-quit-lisp)

      (:desc "eval" :prefix "e"
        :desc "Eval defun"        :n "f" #'slime-eval-defun
        :desc "Eval buffer"       :n "b" #'slime-eval-buffer
        :desc "Eval Region"       :n "r" #'slime-eval-region)

      (:desc "Describe" :prefix "d"
        :desc "Describe Symbol"   :n "s" #'slime-describe-symbol
        :desc "Who calls?"        :n "c" #'slime-who-calls
        :desc "Calls who?"        :n "C" #'slime-calls-who)

      (:desc "Macro" :prefix "m"
        :desc "Macroexpand-all"   :n "E" #'slime-macroexpand-all
        :desc "Macroexpand-1"     :n "e" #'slime-macroexpand-1))

(add-hook 'lisp-mode-hook 'yas-minor-mode-on)

(after! 'doom-org 
  (add-to-list 'org-structure-template-alist
               '("el" "#+BEGIN_SRC emacs-lisp\n?\n#+END_SRC")))
