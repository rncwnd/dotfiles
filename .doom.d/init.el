;;; init.el -*- lexical-binding: t; -*-
;; Copy me to ~/.doom.d/init.el or ~/.config/doom/init.el, then edit me!

(doom! :feature
       debugger          ; stepping through code, to help you add bugs
       eval              ; run code, run (also, repls)
       (evil
        +everywhere)     ; come to the dark side, we have cookies
       snippets          ; my elves. They type so I don't have to
       workspaces        ; tab emulation, persistence & separate workspaces

       :completion
       (company          ; the ultimate code completion backend
        +auto)           ; as-you-type code completion
       (ivy              ; the search engine for love and life
        +fuzzy)          ; FZF

       :ui
       doom              ; what makes DOOM look the way it does
       doom-dashboard    ; a nifty splash screen for Emacs
       doom-quit         ; DOOM quit-message prompts when you quit Emacs
       evil-goggles      ; display visual hints when editing in evil
       hl-todo           ; highlight TODO/FIXME/NOTE tags
       treemacs          ; It's like nerdtree but better!
       (popup            ; tame sudden yet inevitable temporary windows
        +all             ; catch all popups that start with an asterix
        +defaults)       ; default popup rules
       vi-tilde-fringe   ; fringe tildes to mark beyond EOB
       window-select     ; visually switch windows
       vc-gutter         ; Git info like all the meme editors do
       modeline          ; Experimental modeline
       indent-guides     ; Ya

       :emacs
       (dired
        +ranger
        +icons)          ; making dired pretty [functional]
       electric          ; smarter, keyword-based electric-indent
       eshell            ; a consistent, cross-platform shell (WIP)
       imenu             ; an imenu sidebar and searchable code index
       vc                ; Version control yes!

       :tools
       editorconfig      ; let someone else argue about tabs vs spaces
       make              ; run make tasks from Emacs
       rgb               ; Rainbow mode!
       magit             ; Imagine opening a terminal for git
       (pdf
        +modeline)       ; Make \LaTeX even cozier
       flyspell          ; i r spellerer gud
       (flycheck         ; Telling me when i fucked up
        +childframe)
       lsp               ; All the cool kids are using it


       :lang
       assembly          ; assembly for fun or debugging
       (cc
        +irony
        +rtags)          ; C/C++/Obj-C madness
       data              ; config/data formats
       emacs-lisp        ; drown in parentheses
       (haskell
        +intero)         ; a language that's lazier than I am)
       ruby              ; 1.step do {|i| p "Ruby is #{i.even? ? 'love' : 'life'}"}
       sh                ; she sells (ba|z)sh shells on the C xor
       web               ; Makes me want to die
       (python
        +lsp)
               ; Because fuck your opinions, do it this way and this way only
       plantuml          ; Make pretty diagrams for those extra presentation marks

       ; Markups
       (latex
        +okular
        +latexmk
        +pdf-tools=)     ; Writing!
       (org              ; organize your plain life in plain text
        +attach          ; custom attachment system
        +babel           ; running code in org
        +capture         ; org-capture in and outside of Emacs
        +export          ; Exporting org to whatever you want
        +present)        ; Emacs for presentations
       markdown          ; For when redditors try and talk to me

       :editor
       parinfer          ; Make editing lisp a breeze
       fold              ; Vim-like code folding
       rotate-text       ; Cycle between candidate text

       :app
       (write
        +langtool)       ; Fuck word, i want emacs

       :config
       ;; The default module set reasonable defaults for Emacs. It also provides
       ;; a Spacemacs-inspired keybinding scheme, a custom yasnippet library,
       ;; and additional ex commands for evil-mode. Use it as a reference for
       ;; your own modules.
       (default +bindings +evil +evil-bindings +smartparens)
       (literate))
